FROM ruby:alpine
MAINTAINER Justin Hall

RUN mkdir /current

WORKDIR /current

ADD . /current

RUN apk add --update --no-cache \
      build-base \
      nodejs \
      tzdata \
      git \
      libxml2-dev \
      libxslt-dev \
      postgresql-dev


RUN gem update --system
RUN gem install bundler
RUN bundle config build.nokogiri --use-system-libraries

RUN bundle install

EXPOSE 3000
