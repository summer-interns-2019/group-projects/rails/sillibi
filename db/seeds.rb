# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#
#
# assignments = Assignment.create([
# {id: 2,
# name: "Basics of Anchoring a sailboat",
# due_date: Sat, 03 Aug 2019,
# description: "3 page essay on anchoring a sailboat",
# possible_points: 30,
# course_id: 3},
#
# {id: 3,
# name: "Amortization and Depreciation Quiz",
# due_date: Fri, 20 Sep 2019,
# description: "Quiz on accounting topics covered so far",
# possible_points: 100,
# course_id: 7},
#
# {id: 4,
# name: "Sales Final",
# due_date: Sat, 03 Aug 2019,
# description: "Final exam covering all topics from the semester.",
# possible_points: 400,
# course_id: 3}
#                   ])