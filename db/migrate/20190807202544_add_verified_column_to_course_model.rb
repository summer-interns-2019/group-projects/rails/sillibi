class AddVerifiedColumnToCourseModel < ActiveRecord::Migration[5.2]
  def change
    add_column :courses, :verified, :boolean
  end
end
