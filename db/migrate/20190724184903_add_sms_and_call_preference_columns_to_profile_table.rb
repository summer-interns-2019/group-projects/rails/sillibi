class AddSmsAndCallPreferenceColumnsToProfileTable < ActiveRecord::Migration[5.2]
  def change
    add_column :profiles, :sms_preference, :boolean
    add_column :profiles, :email_preference, :boolean
  end
end
