class AddSearchHandleToCourseModel < ActiveRecord::Migration[5.2]
  def change
    add_column :courses, :course_search, :string, add_index: true
  end
end
