class AddSyllabus < ActiveRecord::Migration[5.2]
  def change
    create_table(:syllabuses) do |t|
      t.integer :course_id, unique: true
      t.text :data, limit: 16.megabytes - 1
    end
  end
end
