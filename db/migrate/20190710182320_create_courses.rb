class CreateCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :courses do |t|
      t.string :name
      t.string :number
      t.string :section
      t.string :term
      t.string :instructor
      t.string :color

      t.timestamps
    end
  end
end
