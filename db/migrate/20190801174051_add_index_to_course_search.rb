class AddIndexToCourseSearch < ActiveRecord::Migration[5.2]
  def change
    add_index :courses, :course_search
  end
end
