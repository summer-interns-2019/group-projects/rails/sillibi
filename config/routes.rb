Rails.application.routes.draw do
  devise_for :users,
             path: '',
             path_names: {
                 sign_in: 'login',
                 sign_out: 'logout',
                 registration: 'signup'
             },
             controllers: {
                 sessions: 'sessions',
                 registrations: 'registrations'
             }

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :assignments
  resources :courses do
    collection do
      get :current
    end
    member do
      delete :delete_assignments
    end
  end
  #
  resources :syllabuses
  #
  resources :profiles

  resources :institutions
  resources :courses_users
  resources :users, only: [:index]
  
end