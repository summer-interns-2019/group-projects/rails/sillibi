require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module SillibiRailsApp
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.api_only = true
    config.load_defaults 5.2
    config.autoload_paths += %W(#{config.root}/app/services)

    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'
        resource '*',
                 headers: :any,
                 expose: ['Authorization'],
                 methods: [:get, :post, :options, :delete, :put]
      end
    end
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    config.action_dispatch.default_headers = {
        'Access-Control-Allow-Methods' => %w{POST PUT DELETE GET OPTIONS}.join(","),
        'Access-Control-Allow-Origin' => '*',
        'Access-Control-Request-Method' => '*',
        'Access-Control-Allow-Headers' => '*',
        'Access-Control-Expose-Headers' => '*'
    }
    config.action_controller.forgery_protection_origin_check = false

  end
end
