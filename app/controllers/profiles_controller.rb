class ProfilesController < ApplicationController

  def create
    profile = Profile.create(profile_params)
    render json: profile
  end


  def index
    profile = Profile.where(user_id: current_user.id).first
    email = User.find(current_user.id).email
    institution = User.find(current_user.id).institution.name
    holder = JSON.parse(profile.to_json)
    holder.merge!(email: email, institution: institution)
    render json: holder
  end

  def update
    profile = Profile.where(user_id: current_user.id).first
    puts profile_params
    new_profile = profile.update!(profile_params)
    user = User.find(current_user.id)
    puts user_params
    user.update!(user_params)
    render json: new_profile
  end

  private
  def profile_params
    params.permit(:id, :first_name, :last_name, :phone, :sms_preference, :email_preference, :user_id, :picture)
  end

  def user_params
    params.permit(:email)
  end

end
