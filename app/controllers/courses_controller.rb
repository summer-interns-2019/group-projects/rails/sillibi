class CoursesController < ActiveRecordApi::Rest::Controller
  def model
    @model ||= authorized_models.find_by(id: queryable_params[:id])
    @model.institution_id = current_user.institution_id
    @model
  end
  def current
    render json: current_user.courses
  end

  def delete_assignments
    model.assignments.destroy_all
  end
end
