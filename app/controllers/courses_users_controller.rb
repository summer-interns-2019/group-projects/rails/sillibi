class CoursesUsersController < ActiveRecordApi::Rest::Controller
  def create
    CoursesUser.create!({user_id: current_user.id, course_id: params[:course_id]})
  end
end
