class RegistrationsController < Devise::RegistrationsController
  skip_before_action :authenticate_user!
  respond_to :json

  def create
    build_resource(sign_up_params)

    resource.save
    render_resource(resource)
  end

  def sign_up_params
    user = {
        "email"=>params[:user][:email],
        "password"=>params[:user][:password],
        "institution_id"=>params[:user][:institution_id]
    }
  end
end
