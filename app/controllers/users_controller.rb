class UsersController < ActiveRecordApi::Rest::Controller
  def index
    render json: current_user
  end
end
