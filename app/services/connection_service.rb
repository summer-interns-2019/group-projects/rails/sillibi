require 'faraday'
require 'faraday_middleware'
require 'faraday-http-cache'

class ConnectionService < CredentialsService
  attr_accessor :debug, :path, :cache_store
  attr_writer :full_url

  def authenticated_connection
    connection.headers = connection.headers.merge(headers)
    connection
  end

  def connection
    @connection ||= Faraday.new do |builder|
      builder.options[:open_timeout] = 2
      builder.options[:timeout] = 5
      builder.request :json
      builder.request :url_encoded
      builder.request :retry, max: 5, interval: 0.05, interval_randomness: 0.5, backoff_factor: 2, exceptions: [ActiveRecordApi::Request::AuthError]
      builder.use :http_cache, http_cache_options
      builder.use FaradayMiddleware::FollowRedirects, standards_compliant: true
      builder.response :json, content_type: /\bjson$/
      builder.response :raise_error
      builder.response :logger if debug
      builder.adapter Faraday.default_adapter
    end
  end

  def full_url
    @full_url ||= "#{strip_trailing_slash(host)}#{strip_double_slash("/#{strip_trailing_slash(path)}")}"
  end

  def service_url(suffix)
    parts = full_url.split('/')
    "#{parts[0]}//#{parts[2]}/#{parts[3]}/#{parts[4]}#{strip_double_slash("/#{suffix}")}"
  end

  def full_url_with_params(params: {}, id: nil)
    "#{full_url}/#{id}?#{params.to_query}"
  end

  def symbolize_response(response)
    return response if response.is_a? String
    if response.is_a?(Array)
      symbolize_array_response(response)
    else
      response.try(:deep_symbolize_keys!)
    end
  end

  def symbolize_array_response(response)
    return response unless response.first.is_a? Hash
    response.map!(&:deep_symbolize_keys!)
  end

  def http_cache_options
    http_cache_options = { shared_cache: false, store: cache_store }
    http_cache_options[:logger] = Rails.logger if debug
    http_cache_options
  end

  def strip_double_slash(string)
    string.gsub(%r{//}, '/')
  end

  def strip_trailing_slash(string)
    string.sub(%r{(\/)+$}, '')
  end
end
