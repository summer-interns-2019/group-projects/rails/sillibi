
class ApiService < ConnectionService
  def query(params: {})
    symbolize_response(authenticated_connection.get("#{full_url}/", params).body)
  end

  def get(id:, params: {})
    symbolize_response(authenticated_connection.get(full_url_with_params(id: id, params: params)).body)
  end

  def post(payload:, params: {})
    symbolize_response(authenticated_connection.post(full_url_with_params(params: params), payload).body)
  end

  def put(id:, payload:, params: {})
    symbolize_response(authenticated_connection.put(full_url_with_params(id: id, params: params), payload).body)
  end

end

