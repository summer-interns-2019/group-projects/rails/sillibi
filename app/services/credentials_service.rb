class CredentialsService
  include ActiveAttr::Model
  attr_writer :host, :token_path, :token, :email, :password, :headers

  def token
    @token ||= config(:token)
  end

  def host
    @host ||= config(:host)
  end

  def token_path
    @token_path ||= config(:token_path)
  end

  def email
    @email ||= config(:email)
  end

  def password
    @password ||= config(:password)
  end

  def headers
    @headers ||= {}.tap do |headers|
      if token.present?
        headers['Authorization'] = "Bearer #{token}"
      else
        headers['InjectToken'] = 'true'
      end
    end
  end

  def credentials
    {
      email: email,
      password: password,
      grant_type: 'password'
    }
  end

  def config(name)
    ENV["ACTIVE_RECORD_API_REQUEST_#{name.upcase}"] || credentials_config(name)
  end

  def credentials_config(name)
    return unless defined? Rails
    Rails.application.try(:credentials).try(:active_record_api_request).try(:[], name)
  end
end

