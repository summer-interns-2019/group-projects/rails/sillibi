 class User < ApplicationRecord
   acts_as_token_authenticatable
   belongs_to :institution
   has_one :profile, dependent: :destroy
   has_many :courses_users, dependent: :destroy
   has_many :courses, through: :courses_users

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         :validatable,
         :trackable,
         :jwt_authenticatable,
         :registerable,
         jwt_revocation_strategy: JWTBlacklist

end
