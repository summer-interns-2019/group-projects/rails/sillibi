class Course < ApplicationRecord
  has_many :courses_users, dependent: :destroy
  has_many :users, through: :courses_users
  has_many :assignments, dependent: :destroy
  has_one :syllabus, dependent: :destroy
  belongs_to :institution
end
