class Syllabus < ApplicationRecord
  validates_uniqueness_of :course_id
  belongs_to :course
  after_create :send_to_mturk

  def send_to_mturk
    SyllabusesService.new.post(payload: {external_course_id: self.course.id, data: self.data})
  end
end