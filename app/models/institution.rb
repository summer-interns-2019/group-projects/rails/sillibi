class Institution < ApplicationRecord
  has_many :courses, dependent: :destroy
  has_many :users, dependent: :destroy
end
