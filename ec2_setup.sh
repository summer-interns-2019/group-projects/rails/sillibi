#!/usr/bin/env bash
echo "-------------INSTALLING DOCKER------------------"
# INSTALL DOCKER
# SEE https://docs.docker.com/engine/install/ubuntu/
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get update

sudo apt-get -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

sudo apt-get update
sudo apt-get -y install docker-ce docker-ce-cli containerd.io

echo "-------------GRANT $USER docker access------------------"
echo "-------------DO NOT DO THIS IN PRODUCTION------------------"

sudo gpasswd -a $USER docker
sudo setfacl -m user:$USER:rw /var/run/docker.sock
sudo usermod -aG docker $USER
echo "-------------DO NOT DO THIS IN PRODUCTION------------------"
echo "-------------GRANTED $USER docker access------------------"

sudo docker run hello-world

echo "-------------DOCKER INSTALLED------------------"

echo "-------------INSTALLING DOCKER COMPOSE------------------"
sudo apt-get -y install docker-compose
echo "-------------DOCKER COMPOSE INSTALLED------------------"
echo "-------------UPDATING BASH PROFILE------------------"
